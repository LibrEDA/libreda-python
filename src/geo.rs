// SPDX-FileCopyrightText: 2021 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use libreda_db::prelude as db;
use libreda_db::prelude::ToPolygon;
use pyo3::prelude::*;
use std::fmt;
use std::fmt::Formatter;

/// Base class for geometrical shapes.
#[pyclass(subclass)]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct Geometry {
    // Hack for implementing an enum-like type.
    // Exactly one of the values should be `Some`.
    #[pyo3(get)]
    rect: Option<Rect>,
    #[pyo3(get)]
    simple_polygon: Option<SimplePolygon>,
    #[pyo3(get)]
    polygon: Option<Polygon>,
}

#[pymethods]
impl Geometry {
    /// Convert any geometry to a polygon.
    fn to_polygon(&self) -> Polygon {
        if let Some(r) = &self.rect {
            let r: db::Rect<_> = r.into();
            let p: db::Polygon<_> = r.to_polygon();
            (&p).into()
        } else if let Some(sp) = &self.simple_polygon {
            Polygon::new(sp.clone(), vec![])
        } else if let Some(p) = &self.polygon {
            p.clone()
        } else {
            unreachable!("Unsupported geometry type.")
        }
    }
}

impl From<&db::Geometry<i32>> for Geometry {
    fn from(g: &db::Geometry<i32>) -> Self {
        let mut geo = Geometry {
            rect: None,
            simple_polygon: None,
            polygon: None,
        };
        match g {
            db::Geometry::Point(_) => unimplemented!("Point"),
            db::Geometry::Edge(_) => unimplemented!("Edge"),
            db::Geometry::Rect(r) => {
                geo.rect = Some((*r).into());
            }
            db::Geometry::SimplePolygon(p) => {
                geo.simple_polygon = Some(p.into());
            }
            db::Geometry::SimpleRPolygon(p) => {
                geo.simple_polygon = Some((&p.to_simple_polygon()).into());
            }
            db::Geometry::Polygon(p) => {
                geo.polygon = Some(p.into());
            }
            db::Geometry::Path(_) => unimplemented!("Path"),
            db::Geometry::Text(_) => unimplemented!("Text"),
        };

        geo
    }
}

impl Into<db::Geometry<i32>> for Geometry {
    fn into(self) -> db::Geometry<i32> {
        if let Some(rect) = &self.rect {
            db::Geometry::Rect(rect.into())
        } else if let Some(sp) = &self.simple_polygon {
            db::Geometry::SimplePolygon(sp.into())
        } else if let Some(p) = &self.polygon {
            db::Geometry::Polygon(p.into())
        } else {
            unreachable!("Unsupported geometry type.")
        }
    }
}

/// A point in the euclidean plane.
#[pyclass(subclass)]
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct Point {
    #[pyo3(get, set)]
    x: i32,
    #[pyo3(get, set)]
    y: i32,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let p: db::Point<_> = (*self).into();
        write!(f, "{}", p)
    }
}

impl From<db::Point<i32>> for Point {
    fn from(p: db::Point<i32>) -> Self {
        Self::new(p.x, p.y)
    }
}

impl Into<db::Point<i32>> for Point {
    fn into(self) -> db::Point<i32> {
        db::Point::new(self.x, self.y)
    }
}

#[pymethods]
impl Point {
    #[new]
    pub fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    fn __repr__(&self) -> String {
        format!("{}", self)
    }

    fn __str__(&self) -> String {
        format!("{}", self)
    }
}

/// A polygon without holes, possibly self-intersecting.
#[pyclass(subclass)]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct SimplePolygon {
    #[pyo3(get, set)]
    points: Vec<Point>,
}

impl From<&db::SimplePolygon<i32>> for SimplePolygon {
    fn from(p: &db::SimplePolygon<i32>) -> Self {
        Self::new(p.iter().map(|&p| p.into()).collect())
    }
}

impl Into<db::SimplePolygon<i32>> for &SimplePolygon {
    fn into(self) -> db::SimplePolygon<i32> {
        db::SimplePolygon::new(self.points.iter().map(|&p| p.into()).collect())
    }
}

#[pymethods]
impl SimplePolygon {
    #[new]
    pub fn new(points: Vec<Point>) -> SimplePolygon {
        Self { points }
    }
}

/// A polygon without holes, possibly self-intersecting.
#[pyclass(subclass)]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct Polygon {
    #[pyo3(get, set)]
    exterior: SimplePolygon,
    #[pyo3(get, set)]
    interiors: Vec<SimplePolygon>,
}

impl From<&db::Polygon<i32>> for Polygon {
    fn from(p: &db::Polygon<i32>) -> Self {
        Self::new(
            (&p.exterior).into(),
            p.interiors.iter().map(|p| p.into()).collect(),
        )
    }
}

impl Into<db::Polygon<i32>> for &Polygon {
    fn into(self) -> db::Polygon<i32> {
        db::Polygon {
            exterior: (&self.exterior).into(),
            interiors: self.interiors.iter().map(|p| p.into()).collect(),
        }
        .normalized()
    }
}

#[pymethods]
impl Polygon {
    #[new]
    #[pyo3(signature = (exterior, interiors = vec![]))]
    pub fn new(exterior: SimplePolygon, interiors: Vec<SimplePolygon>) -> Polygon {
        Polygon {
            exterior,
            interiors,
        }
    }

    #[staticmethod]
    fn from_points(points: Vec<Point>) -> Self {
        Self::new(SimplePolygon::new(points), vec![])
    }
}

/// An axis-aligned rectangle.
#[pyclass]
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct Rect {
    #[pyo3(get, set)]
    lower_left: Point,
    #[pyo3(get, set)]
    upper_right: Point,
}

impl From<db::Rect<i32>> for Rect {
    fn from(r: db::Rect<i32>) -> Self {
        Self::new(r.lower_left.into(), r.upper_right.into())
    }
}

impl Into<db::Rect<i32>> for &Rect {
    fn into(self) -> db::Rect<i32> {
        db::Rect::new(self.lower_left, self.upper_right)
    }
}

#[pymethods]
impl Rect {
    #[new]
    pub fn new(p1: Point, p2: Point) -> Rect {
        db::Rect::new(p1, p2).into()
    }

    fn __repr__(&self) -> String {
        let r: db::Rect<i32> = self.into();
        format!("{:?}", r)
    }

    fn __str__(&self) -> String {
        self.__repr__()
    }
}

/// Geometric 'similarity' transform.
#[pyclass(subclass)]
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct SimpleTransform {
    #[pyo3(get, set)]
    displacement: Point,
    #[pyo3(get, set)]
    mirror: bool,
    /// Rotation by a multiple of 90 degrees.
    #[pyo3(get, set)]
    rotation: i32,
    #[pyo3(get, set)]
    magnification: i32,
}

impl From<db::SimpleTransform<i32>> for SimpleTransform {
    fn from(tf: db::SimpleTransform<i32>) -> Self {
        Self {
            displacement: db::Point::from(tf.displacement).into(),
            mirror: tf.mirror,
            rotation: tf.rotation.as_int() as i32,
            magnification: tf.magnification,
        }
    }
}

impl Into<db::SimpleTransform<i32>> for SimpleTransform {
    fn into(self) -> db::SimpleTransform<i32> {
        let disp: db::Point<_> = self.displacement.into();
        db::SimpleTransform {
            mirror: self.mirror,
            rotation: db::Angle::from_u32((self.rotation % 4 + 4) as u32),
            magnification: self.magnification,
            displacement: disp.v(),
        }
    }
}

#[pymethods]
impl SimpleTransform {
    #[new]
    pub fn new() -> Self {
        db::SimpleTransform::identity().into()
    }

    #[staticmethod]
    pub fn translation(p: Point) -> Self {
        let p: db::Point<_> = p.into();
        let p: db::Point<_> = p.into();
        db::SimpleTransform::translate(p).into()
    }

    fn __repr__(&self) -> String {
        format!(
            "Transform(mirror={:?}, rotation={}, displacement={}, magnification={})",
            self.mirror, self.rotation, self.displacement, self.magnification
        )
    }

    fn __str__(&self) -> String {
        self.__repr__()
    }
}
