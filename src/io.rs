// SPDX-FileCopyrightText: 2021 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::chip::Chip;
use libreda_db::prelude::{LayoutStreamReader, LayoutStreamWriter};
use libreda_db::prelude::{NetlistReader, NetlistWriter};
use libreda_oasis::*;
use libreda_structural_verilog::*;
use pyo3::prelude::*;

use pyo3::exceptions::PyException;
use std::fs;
use std::io::{BufReader, BufWriter};

#[pyclass]
pub(crate) struct VerilogReader {
    reader: StructuralVerilogReader,
}

/// Reader for structural verilog.
#[pymethods]
impl VerilogReader {
    #[new]
    #[pyo3(signature = (load_blackboxes = false))]
    fn new(load_blackboxes: bool) -> Self {
        let reader = StructuralVerilogReader::new().load_blackboxes(load_blackboxes);
        VerilogReader { reader }
    }

    /// Read a netlist from a structural Verilog file.
    fn read_netlist(&self, path: &str) -> PyResult<Chip> {
        let f = fs::File::open(path)?;
        let mut buf_reader = BufReader::new(f);
        let chip = self.reader.read_netlist(&mut buf_reader).map_err(|err| {
            let msg = format!("{:?}", err);
            PyException::new_err(msg)
        })?;

        Ok(Chip { chip })
    }

    /// Populate a netlist with the contents of a structural Verilog file.
    fn read_into_netlist(&self, path: &str, chip: &mut Chip) -> PyResult<()> {
        let f = fs::File::open(path)?;
        let mut buf_reader = BufReader::new(f);
        self.reader
            .read_into_netlist(&mut buf_reader, &mut chip.chip)
            .map_err(|err| {
                let msg = format!("{:?}", err);
                PyException::new_err(msg)
            })?;

        Ok(())
    }
}

#[pyclass]
pub(crate) struct VerilogWriter {
    writer: StructuralVerilogWriter,
}
/// Writer for structural verilog.
#[pymethods]
impl VerilogWriter {
    #[new]
    fn new() -> Self {
        let writer = StructuralVerilogWriter::new();
        VerilogWriter { writer }
    }

    /// Write a netlist to a structural Verilog file.
    fn write_netlist(&self, chip: &Chip, path: &str) -> PyResult<()> {
        let f = fs::File::create(path)?;
        let mut buf_writer = BufWriter::new(f);
        self.writer
            .write_netlist(&mut buf_writer, &chip.chip)
            .map_err(|err| {
                let msg = format!("{:?}", err);
                PyException::new_err(msg)
            })?;

        Ok(())
    }
}

/// Reader for the OASIS layout file format.
#[pyclass]
pub(crate) struct OASISReader {
    reader: OASISStreamReader,
}

#[pymethods]
impl OASISReader {
    /// Create a new default reader for the OASIS file format.
    #[new]
    fn new() -> Self {
        let reader = OASISStreamReader::new();
        OASISReader { reader }
    }

    /// Read a layout from an OASIS file.
    fn read_layout(&self, path: &str) -> PyResult<Chip> {
        let mut chip = Chip::new();
        self.read_into_layout(path, &mut chip)?;
        Ok(chip)
    }

    /// Populate a layout with the contents of an OASIS file.
    fn read_into_layout(&self, path: &str, chip: &mut Chip) -> PyResult<()> {
        let f = fs::File::open(path)?;
        let mut buf_reader = BufReader::new(f);
        self.reader
            .read_layout(&mut buf_reader, &mut chip.chip)
            .map_err(|err| {
                let msg = format!("{:?}", err);
                PyException::new_err(msg)
            })?;

        Ok(())
    }
}

#[pyclass]
pub(crate) struct OASISWriter {
    writer_config: OASISWriterConfig,
}
/// Writer for structural verilog.
#[pymethods]
impl OASISWriter {
    #[new]
    fn new() -> Self {
        let writer_config = OASISWriterConfig::default();
        OASISWriter { writer_config }
    }

    /// Write a layout to an OASIS file.
    fn write_layout(&self, chip: &Chip, path: &str) -> PyResult<()> {
        let f = fs::File::create(path)?;
        let mut buf_writer = BufWriter::new(f);

        let writer = OASISStreamWriter::with_config(self.writer_config.clone());

        writer
            .write_layout(&mut buf_writer, &chip.chip)
            .map_err(|err| {
                let msg = format!("{:?}", err);
                PyException::new_err(msg)
            })?;

        Ok(())
    }
}
