// SPDX-FileCopyrightText: 2021 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Use a Python chip object from Rust.

use libreda_db::prelude::*;
use pyo3::types::PyTuple;
use pyo3::{FromPyObject, IntoPy, Py, PyAny, PyObject, PyResult, Python};
use std::hash::{Hash, Hasher};

use crate::geo;

/// Wrapper around a Python chip data-structure.
pub struct PyChip<'a> {
    base: &'a PyAny,
}

impl<'a> PyChip<'a> {
    pub fn new(base: &'a PyAny) -> Self {
        Self { base }
    }

    /// Call a function and convert the return object into `T`.
    /// # Panics
    /// Panics if the function-call or the conversion fails.
    fn call_function_and_convert<'p, T, Args>(&'p self, function_name: &str, args: Args) -> T
    where
        T: FromPyObject<'p>,
        Args: IntoPy<Py<PyTuple>>,
    {
        let ret = self
            .base
            .call_method1(function_name, args)
            .expect("Failed to call function.");
        ret.extract()
            .expect("Failed to convert Python value into Rust value.")
    }

    /// Call single-argument function where the only argument is an ID object.
    fn call1_function_and_convert<'p, T>(&'p self, function_name: &str, arg: &PyIdObject) -> T
    where
        T: FromPyObject<'p>,
    {
        let py = self.base.py();
        let args = (arg.obj.clone_ref(py),);
        self.call_function_and_convert(function_name, args)
    }

    /// Call two-argument function where the two arguments are ID objects.
    fn call2_function_and_convert<'p, T>(
        &'p self,
        function_name: &str,
        arg1: &PyIdObject,
        arg2: &PyIdObject,
    ) -> T
    where
        T: FromPyObject<'p>,
    {
        let py = self.base.py();
        let args = (arg1.obj.clone_ref(py), arg2.obj.clone_ref(py));
        self.call_function_and_convert(function_name, args)
    }
}

impl<'a> HierarchyIds for PyChip<'a> {
    type CellId = PyIdObject;
    type CellInstId = PyIdObject;
}

impl<'a> HierarchyBase for PyChip<'a> {
    type NameType = String;

    fn cell_by_name(&self, name: &str) -> Option<Self::CellId> {
        self.call_function_and_convert("cell_by_name", (name.to_string(),))
    }

    fn cell_instance_by_name(
        &self,
        parent_cell: &Self::CellId,
        name: &str,
    ) -> Option<Self::CellInstId> {
        let py = self.base.py();
        let args = (parent_cell.obj.clone_ref(py), name.to_string());
        self.call_function_and_convert("cell_by_name", args)
    }

    fn cell_name(&self, cell: &Self::CellId) -> Self::NameType {
        self.call1_function_and_convert("cell_name", cell)
    }

    fn cell_instance_name(&self, cell_inst: &Self::CellInstId) -> Option<Self::NameType> {
        self.call1_function_and_convert("cell_instance_name", cell_inst)
    }

    fn parent_cell(&self, cell_inst: &Self::CellInstId) -> Self::CellId {
        self.call1_function_and_convert("parent_cell", cell_inst)
    }

    fn template_cell(&self, cell_inst: &Self::CellInstId) -> Self::CellId {
        self.call1_function_and_convert("template_cell", cell_inst)
    }

    fn for_each_cell<F>(&self, f: F)
    where
        F: FnMut(Self::CellId) -> (),
    {
        self.each_cell().for_each(f)
    }

    fn each_cell_vec(&self) -> Vec<Self::CellId> {
        self.call_function_and_convert("each_cell_vec", ())
    }

    fn for_each_cell_instance<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellInstId) -> (),
    {
        self.each_cell_instance(cell).for_each(f)
    }

    fn each_cell_instance_vec(&self, cell: &Self::CellId) -> Vec<Self::CellInstId> {
        self.call1_function_and_convert("each_cell_instance_vec", cell)
    }

    fn for_each_cell_dependency<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellId) -> (),
    {
        self.each_cell_dependency(cell).for_each(f)
    }

    fn each_cell_dependency_vec(&self, cell: &Self::CellId) -> Vec<Self::CellId> {
        self.call1_function_and_convert("each_cell_dependency_vec", cell)
    }

    fn num_cell_dependencies(&self, cell: &Self::CellId) -> usize {
        self.call1_function_and_convert("num_cell_dependencies", cell)
    }

    fn for_each_dependent_cell<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellId) -> (),
    {
        self.each_dependent_cell(cell).for_each(f)
    }

    fn each_dependent_cell_vec(&self, cell: &Self::CellId) -> Vec<Self::CellId> {
        self.call1_function_and_convert("each_dependent_cell_vec", cell)
    }

    fn num_dependent_cells(&self, cell: &Self::CellId) -> usize {
        self.call1_function_and_convert("num_dependent_cells", cell)
    }

    fn for_each_cell_reference<F>(&self, cell: &Self::CellId, f: F)
    where
        F: FnMut(Self::CellInstId) -> (),
    {
        self.each_cell_reference(cell).for_each(f)
    }

    fn each_cell_reference_vec(&self, cell: &Self::CellId) -> Vec<Self::CellInstId> {
        self.call1_function_and_convert("each_cell_reference_vec", cell)
    }

    fn num_cell_references(&self, cell: &Self::CellId) -> usize {
        self.call1_function_and_convert("num_cell_references", cell)
    }

    fn num_child_instances(&self, cell: &Self::CellId) -> usize {
        self.call1_function_and_convert("num_child_instances", cell)
    }

    fn num_cells(&self) -> usize {
        self.call_function_and_convert("num_cells", ())
    }

    fn get_chip_property(&self, key: &Self::NameType) -> Option<PropertyValue> {
        unimplemented!()
    }

    fn get_cell_property(
        &self,
        cell: &Self::CellId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        unimplemented!()
    }

    fn get_cell_instance_property(
        &self,
        inst: &Self::CellInstId,
        key: &Self::NameType,
    ) -> Option<PropertyValue> {
        unimplemented!()
    }
}

impl<'a> LayoutIds for PyChip<'a> {
    type Coord = i32;
    type Area = i64;
    type LayerId = PyIdObject;
    type ShapeId = PyIdObject;
}

impl<'a> LayoutBase for PyChip<'a> {
    fn dbu(&self) -> Self::Coord {
        self.call_function_and_convert("dbu", ())
    }

    fn each_layer(&self) -> Box<dyn Iterator<Item = Self::LayerId>> {
        let layers: Vec<Self::LayerId> = self.call_function_and_convert("each_layer", ());
        Box::new(layers.into_iter())
    }

    fn layer_info(&self, layer: &Self::LayerId) -> LayerInfo<Self::NameType> {
        let info: crate::chip::LayerInfo = self.call1_function_and_convert("layer_info", layer);
        info.into()
    }

    fn find_layer(&self, index: u32, datatype: u32) -> Option<Self::LayerId> {
        self.call_function_and_convert("find_layer", (index, datatype))
    }

    fn layer_by_name(&self, name: &str) -> Option<Self::LayerId> {
        self.call_function_and_convert("layer_by_name", (name.to_string(),))
    }

    fn bounding_box_per_layer(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Option<Rect<Self::Coord>> {
        let bbox: Option<geo::Rect> =
            self.call2_function_and_convert("bounding_box_per_layer", cell, layer);
        bbox.map(|r| (&r).into())
    }

    fn each_shape_id(
        &self,
        cell: &Self::CellId,
        layer: &Self::LayerId,
    ) -> Box<dyn Iterator<Item = Self::ShapeId>> {
        let shapes: Vec<Self::ShapeId> =
            self.call2_function_and_convert("each_shape_id", cell, layer);
        Box::new(shapes.into_iter())
    }

    fn for_each_shape<F>(&self, cell: &Self::CellId, layer: &Self::LayerId, f: F)
    where
        F: FnMut(&Self::ShapeId, &Geometry<Self::Coord>) -> (),
    {
        unimplemented!()
    }

    fn with_shape<F, R>(&self, shape_id: &Self::ShapeId, f: F) -> R
    where
        F: FnMut(&Self::LayerId, &Geometry<Self::Coord>) -> R,
    {
        unimplemented!()
    }

    fn parent_of_shape(&self, shape_id: &Self::ShapeId) -> (Self::CellId, Self::LayerId) {
        self.call1_function_and_convert("parent_of_shape", shape_id)
    }

    fn get_transform(&self, cell_inst: &Self::CellInstId) -> SimpleTransform<Self::Coord> {
        let tf: geo::SimpleTransform = self.call1_function_and_convert("get_transform", cell_inst);
        tf.into()
    }
}

impl<'a> NetlistIds for PyChip<'a> {
    type PinId = PyIdObject;
    type PinInstId = PyIdObject;
    type NetId = PyIdObject;
}

impl<'a> NetlistBase for PyChip<'a> {
    fn template_pin(&self, pin_instance: &Self::PinInstId) -> Self::PinId {
        let py = self.base.py();
        let args = (pin_instance.obj.clone_ref(py),);
        let obj = self.call_function_and_convert("template_pin", args);
        PyIdObject { obj }
    }

    fn pin_direction(&self, pin: &Self::PinId) -> Direction {
        let dir: crate::chip::Direction = self.call1_function_and_convert("pin_name", pin);
        dir.into()
    }

    fn pin_name(&self, pin: &Self::PinId) -> Self::NameType {
        self.call1_function_and_convert("pin_name", pin)
    }

    fn pin_by_name(&self, parent_cell: &Self::CellId, name: &str) -> Option<Self::PinId> {
        let py = self.base.py();
        let args = (parent_cell.obj.clone_ref(py), name.to_string());
        self.call_function_and_convert("pin_by_name", args)
    }

    fn parent_cell_of_pin(&self, pin: &Self::PinId) -> Self::CellId {
        self.call1_function_and_convert("parent_cell_of_pin", pin)
    }

    fn parent_of_pin_instance(&self, pin_inst: &Self::PinInstId) -> Self::CellInstId {
        self.call1_function_and_convert("parent_of_pin_instance", pin_inst)
    }

    fn parent_cell_of_net(&self, net: &Self::NetId) -> Self::CellId {
        self.call1_function_and_convert("parent_cell_of_net", net)
    }

    fn net_of_pin(&self, pin: &Self::PinId) -> Option<Self::NetId> {
        self.call1_function_and_convert("net_of_pin", pin)
    }

    fn net_of_pin_instance(&self, pin_instance: &Self::PinInstId) -> Option<Self::NetId> {
        self.call1_function_and_convert("net_of_pin_instance", pin_instance)
    }

    fn net_zero(&self, parent_circuit: &Self::CellId) -> Self::NetId {
        self.call1_function_and_convert("net_zero", parent_circuit)
    }

    fn net_one(&self, parent_circuit: &Self::CellId) -> Self::NetId {
        self.call1_function_and_convert("net_one", parent_circuit)
    }

    fn net_by_name(&self, parent_circuit: &Self::CellId, name: &str) -> Option<Self::NetId> {
        let py = self.base.py();
        let args = (parent_circuit.obj.clone_ref(py), name.to_string());
        self.call_function_and_convert("net_by_name", args)
    }

    fn net_name(&self, net: &Self::NetId) -> Option<Self::NameType> {
        self.call1_function_and_convert("net_name", net)
    }

    fn for_each_pin<F>(&self, circuit: &Self::CellId, f: F)
    where
        F: FnMut(Self::PinId) -> (),
    {
        self.each_pin(circuit).for_each(f)
    }

    fn each_pin_vec(&self, circuit: &Self::CellId) -> Vec<Self::PinId> {
        self.call1_function_and_convert("each_pin_vec", circuit)
    }

    fn for_each_pin_instance<F>(&self, circuit_inst: &Self::CellInstId, f: F)
    where
        F: FnMut(Self::PinInstId) -> (),
    {
        self.each_pin_instance(circuit_inst).for_each(f)
    }

    fn each_pin_instance_vec(&self, circuit_instance: &Self::CellInstId) -> Vec<Self::PinInstId> {
        self.call1_function_and_convert("each_pin_instance_vec", circuit_instance)
    }

    fn for_each_internal_net<F>(&self, circuit: &Self::CellId, f: F)
    where
        F: FnMut(Self::NetId) -> (),
    {
        self.each_internal_net(circuit).for_each(f)
    }

    fn each_internal_net_vec(&self, circuit: &Self::CellId) -> Vec<Self::NetId> {
        self.call1_function_and_convert("each_internal_net_vec", circuit)
    }

    fn num_pins(&self, circuit: &Self::CellId) -> usize {
        self.call1_function_and_convert("num_pins", circuit)
    }

    fn for_each_pin_of_net<F>(&self, net: &Self::NetId, f: F)
    where
        F: FnMut(Self::PinId) -> (),
    {
        self.each_pin_of_net(net).for_each(f)
    }

    fn each_pin_of_net_vec(&self, net: &Self::NetId) -> Vec<Self::PinId> {
        self.call1_function_and_convert("each_pin_of_net_vec", net)
    }

    fn for_each_pin_instance_of_net<F>(&self, net: &Self::NetId, f: F)
    where
        F: FnMut(Self::PinInstId) -> (),
    {
        self.each_pin_instance_of_net(net).for_each(f)
    }

    fn each_pin_instance_of_net_vec(&self, net: &Self::NetId) -> Vec<Self::PinInstId> {
        self.call1_function_and_convert("each_pin_instance_of_net_vec", net)
    }
}

impl<'a> L2NBase for PyChip<'a> {
    fn shapes_of_net(&self, net_id: &Self::NetId) -> Box<dyn Iterator<Item = Self::ShapeId>> {
        let vec: Vec<_> = self.call1_function_and_convert("shapes_of_net", net_id);
        Box::new(vec.into_iter())
    }

    fn shapes_of_pin(&self, pin_id: &Self::PinId) -> Box<dyn Iterator<Item = Self::ShapeId>> {
        let vec: Vec<_> = self.call1_function_and_convert("shapes_of_pin", pin_id);
        Box::new(vec.into_iter())
    }

    fn get_net_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::NetId> {
        self.call1_function_and_convert("get_net_of_shape", shape_id)
    }

    fn get_pin_of_shape(&self, shape_id: &Self::ShapeId) -> Option<Self::PinId> {
        self.call1_function_and_convert("get_pin_of_shape", shape_id)
    }
}

/// Use a Python object as an ID type.
#[derive(Clone, Debug)]
pub struct PyIdObject {
    obj: PyObject,
}

impl Eq for PyIdObject {}

impl PartialEq for PyIdObject {
    fn eq(&self, other: &Self) -> bool {
        Python::with_gil(|py| self.obj.as_ref(py).eq(other.obj.as_ref(py))).unwrap_or(false)
    }
}

impl Hash for PyIdObject {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let h: isize = Python::with_gil(|py| {
            self.obj
                .as_ref(py)
                .hash()
                .expect("`hash()` does not work as intended on ID object.")
        });
        h.hash(state)
    }
}

impl<'source> FromPyObject<'source> for PyIdObject {
    fn extract(any: &'source PyAny) -> PyResult<Self> {
        let obj = Python::with_gil(|py| any.into_py(py));
        Ok(PyIdObject { obj })
    }
}
