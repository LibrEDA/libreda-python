// SPDX-FileCopyrightText: 2021 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Python binding for LibrEDA.

mod chip;
mod geo;
mod io;
mod pychip;

use libreda_db::prelude::reference_access::*;
use libreda_db::traits::*;
use pyo3::prelude::*;

use crate::pychip::PyChip;
use chip::*;

#[pyfunction]
fn version() -> String {
    env!("CARGO_PKG_VERSION").to_string()
}

#[pyfunction]
fn print_hierarchy(chip: &PyAny) {
    println!("print_hierarchy");
    println!("dir = {:?}", chip.dir());
    let pychip = PyChip::new(chip);

    do_print_hierarchy(&pychip)
}

fn do_print_hierarchy<H: HierarchyBase>(h: &H) {
    for cell in h.each_cell_ref() {
        println!(
            "{} (#total uses: {}, #child instances: {})",
            cell.name(),
            cell.num_references(),
            cell.num_child_instances()
        );
        for inst in cell.each_cell_instance() {
            println!("  - {} {:?}", inst.template().name(), inst.name())
        }
        println!()
    }
}

// #[pyclass]
// struct MyIterator {
//     iter: Box<dyn Iterator<Item=PyObject> + Send>,
// }
//
// #[pyproto]
// impl PyIterProtocol for MyIterator {
//     fn __iter__(slf: PyRef<Self>) -> Py<MyIterator> {
//         slf.into()
//     }
//     fn __next__(mut slf: PyRefMut<Self>) -> Option<PyObject> {
//         slf.iter.next()
//     }
// }

#[pymodule]
fn libreda(py: Python, m: &PyModule) -> PyResult<()> {
    // Add constants.
    // m.add("version", env!("CARGO_PKG_VERSION"))?;

    // Add module functions.
    {
        m.add_function(wrap_pyfunction!(version, m)?)?;
        m.add_function(wrap_pyfunction!(print_hierarchy, m)?)?;
    }

    // Add classes.
    {
        // m.add_class::<Hierarchy>()?;
        // m.add_class::<Netlist>()?;
        m.add_class::<Chip>()?;
        m.add_class::<CellId>()?;
        m.add_class::<CellInstId>()?;
        m.add_class::<chip::TerminalId>()?;
        m.add_class::<NetId>()?;
        m.add_class::<PinId>()?;
        m.add_class::<PinInstId>()?;
        m.add_class::<Direction>()?;
        m.add_class::<LayerInfo>()?;
    }

    // Add submodules.

    // IO (input-output)
    {
        let io = PyModule::new(py, "io")?;
        io.add_class::<io::VerilogReader>()?;
        io.add_class::<io::VerilogWriter>()?;
        io.add_class::<io::OASISReader>()?;
        io.add_class::<io::OASISWriter>()?;
        m.add_submodule(io)?;
    }

    // geo (geometry)
    {
        let geo = PyModule::new(py, "geo")?;
        geo.add_class::<geo::Point>()?;
        geo.add_class::<geo::Geometry>()?;
        geo.add_class::<geo::Rect>()?;
        geo.add_class::<geo::SimplePolygon>()?;
        geo.add_class::<geo::Polygon>()?;
        geo.add_class::<geo::SimpleTransform>()?;
        m.add_submodule(geo)?;
    }

    Ok(())
}
