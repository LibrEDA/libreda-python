<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# libreda-python

*Work-in-progress*

Python binding for [libreda-db](https://codeberg.org/LibrEDA/libreda-db).

## Install

Create a Python virtual environment:
```sh
python -m venv myPythonEnv
source myPythonEnv/bin/activate

# Install maturin (build tool for Rust-based Python packages)
pip install maturin
```

Download and install libreda-python:

```sh
# LibrEDA consists of many Rust libraries. The workspace bundles them together.
git clone --recursive https://codeberg.org/LibrEDA/libreda-rs-workspace libreda
cd libreda/libreda-python

# Install in development mode.
maturin develop
```

Run a Python shell and import `libreda`:
```sh
python
```

```python
import libreda as db

chip = db.Chip()
my_cell = chip.create_cell("A")

# Write verilog.
writer = db.io.VerilogWriter()
writer.write_netlist(chip, "output.v")

# Read verilog.
reader = db.io.VerilogReader()
netlist = reader.read_netlist("output.v")
```
