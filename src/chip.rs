// SPDX-FileCopyrightText: 2021 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use db::traits::*;
use libreda_db::prelude as db;

use std::fmt;
use std::ops::Deref;

use crate::geo::*;
use pyo3::prelude::*;

// /// Cell hierarchy.
// #[pyclass(subclass)]
// pub struct Hierarchy {
//     pub(crate) chip: db::Chip<db::Coord>,
// }
//
// /// Netlist data-structure.
// #[pyclass(subclass, extends=Hierarchy)]
// pub struct Netlist {
// }
//
// /// Layout data-structure.
// #[pyclass(subclass, extends=Hierarchy)]
// pub struct Layout {
// }

/// Fused layout and netlist.
#[pyclass(subclass)]
pub struct Chip {
    pub(crate) chip: db::Chip<db::Coord>,
}

/// IO direction.
///
#[pyclass]
#[derive(Default, Copy, Clone, Hash, PartialEq, Eq)]
pub struct Direction {
    #[pyo3(get, set)]
    is_input: bool,
    #[pyo3(get, set)]
    is_output: bool,
    // #[pyo3(get, set)]
    // is_analog: bool,
    #[pyo3(get, set)]
    is_ground: bool,
    #[pyo3(get, set)]
    is_supply: bool,
}

#[pymethods]
impl Direction {
    #[staticmethod]
    fn input() -> Direction {
        Self {
            is_input: true,
            ..Self::default()
        }
    }

    #[staticmethod]
    fn output() -> Direction {
        Self {
            is_output: true,
            ..Self::default()
        }
    }

    #[staticmethod]
    fn inout() -> Direction {
        Self {
            is_output: true,
            is_input: true,
            ..Self::default()
        }
    }
}

impl Into<db::Direction> for Direction {
    fn into(self) -> db::Direction {
        match self {
            Direction {
                is_input: true,
                is_output: false,
                ..
            } => db::Direction::Input,
            Direction {
                is_input: false,
                is_output: true,
                ..
            } => db::Direction::Output,
            Direction {
                is_input: true,
                is_output: true,
                ..
            } => db::Direction::InOut,
            Direction { .. } => db::Direction::None,
        }
    }
}

impl From<db::Direction> for Direction {
    fn from(d: db::Direction) -> Self {
        let mut output = Direction::default();
        match d {
            db::Direction::None => {}
            db::Direction::Input => output.is_input = true,
            db::Direction::Output => output.is_output = true,
            db::Direction::InOut => {
                output.is_input = true;
                output.is_output = true;
            }
            db::Direction::Clock => {}
            db::Direction::Supply => output.is_supply = true,
            db::Direction::Ground => output.is_ground = true,
        }

        output
    }
}

/// Wrap rust IDs into Python objects.
macro_rules! id_wrapper {
    ($b:tt, $t:tt) => {
        /// ID wrapper for $t.
        #[pyclass]
        #[derive(Clone, Hash, PartialEq, Eq)]
        pub struct $t {
            id: <db::Chip as $b>::$t,
        }

        impl Deref for $t {
            type Target = <db::Chip as $b>::$t;

            fn deref(&self) -> &Self::Target {
                &self.id
            }
        }

        // impl $t {
        //     fn from(id: <db::Chip as $b>::$t) -> $t {
        //         $t {id}
        //     }
        //
        //     fn into(self) -> <db::Chip as $b>::$t {
        //         self.id
        //     }
        // }
    };
}

id_wrapper!(HierarchyIds, CellId);
id_wrapper!(HierarchyIds, CellInstId);
id_wrapper!(NetlistIds, NetId);
id_wrapper!(NetlistIds, PinId);
id_wrapper!(NetlistIds, PinInstId);
id_wrapper!(LayoutIds, LayerId);
id_wrapper!(LayoutIds, ShapeId);

/// ID wrapper for TerminalID.
/// A terminal can either be a pin or a pin instance.
#[pyclass]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct TerminalId {
    id: db::TerminalId<db::Chip>,
}

impl Deref for TerminalId {
    type Target = db::TerminalId<db::Chip>;

    fn deref(&self) -> &Self::Target {
        &self.id
    }
}

#[pymethods]
impl TerminalId {
    /// Get the pin ID if the terminal is a pin, otherwise return `None`.
    fn get_pin(&self) -> Option<PinId> {
        match self.id {
            db::TerminalId::PinId(id) => Some(PinId { id }),
            db::TerminalId::PinInstId(_) => None,
        }
    }

    /// Get the pin instance ID if the terminal is a pin instance, otherwise return `None`.
    fn get_pin_instance(&self) -> Option<PinInstId> {
        match self.id {
            db::TerminalId::PinId(_) => None,
            db::TerminalId::PinInstId(id) => Some(PinInstId { id }),
        }
    }

    /// Test if the terminal is a pin.
    fn is_pin(&self) -> bool {
        self.get_pin().is_some()
    }

    /// Test if the terminal is a pin instance.
    fn is_pin_instance(&self) -> bool {
        self.get_pin_instance().is_some()
    }
}

#[pyclass]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct LayerInfo {
    #[pyo3(get, set)]
    index: u32,
    #[pyo3(get, set)]
    data_type: u32,
    #[pyo3(get, set)]
    name: Option<String>,
}

impl<T> Into<db::LayerInfo<T>> for LayerInfo
where
    T: From<String>,
{
    fn into(self) -> db::LayerInfo<T> {
        db::LayerInfo {
            index: self.index,
            datatype: self.data_type,
            name: self.name.map(|n| n.into()),
        }
    }
}

impl fmt::Display for Chip {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.chip.fmt(f)
    }
}

#[pymethods]
impl Chip {
    // == HierarchyBase & HierarchyEdit ==

    /// Create an empty netlist/layout structure.
    #[new]
    pub fn new() -> Self {
        Chip {
            chip: db::Chip::new(),
        }
    }

    fn __repr__(&self) -> String {
        format!("{}", self)
    }

    /// Get the number of cells.
    fn num_cells(&self) -> usize {
        self.chip.num_cells()
    }

    /// Create a new cell.
    fn create_cell(&mut self, name: String) -> PyResult<CellId> {
        let id = self.chip.create_cell(name.into());
        Ok(CellId { id })
    }

    fn remove_cell(&mut self, cell: &CellId) -> PyResult<()> {
        self.chip.remove_cell(cell);
        Ok(())
    }

    fn rename_cell(&mut self, cell: &CellId, new_name: String) -> PyResult<()> {
        self.chip.rename_cell(cell, new_name.into());
        Ok(())
    }

    fn cell_name(&self, cell: &CellId) -> PyResult<String> {
        Ok(self.chip.cell_name(cell).to_string())
    }

    fn cell_by_name(&self, name: &str) -> Option<CellId> {
        self.chip.cell_by_name(name).map(|id| CellId { id })
    }

    fn cell_instance_by_name(&self, parent: &CellId, name: &str) -> Option<CellInstId> {
        self.chip
            .cell_instance_by_name(parent, name)
            .map(|id| CellInstId { id })
    }

    /// Get the number of child cell instances inside `cell`.
    fn num_child_instances(&self, cell: &CellId) -> usize {
        self.chip.num_child_instances(cell)
    }

    fn create_cell_instance(
        &mut self,
        parent: &CellId,
        template: &CellId,
        name: Option<String>,
    ) -> PyResult<CellInstId> {
        let id = self
            .chip
            .create_cell_instance(parent, template, name.map(|n| n.into()));
        Ok(CellInstId { id })
    }

    fn remove_cell_instance(&mut self, inst: &CellInstId) -> PyResult<()> {
        self.chip.remove_cell_instance(inst);
        Ok(())
    }

    fn rename_cell_instance(&mut self, inst: &CellInstId, new_name: String) -> PyResult<()> {
        self.chip.rename_cell_instance(inst, new_name.into());
        Ok(())
    }

    fn cell_instance_name(&self, instance: &CellInstId) -> PyResult<Option<String>> {
        Ok(self
            .chip
            .cell_instance_name(instance)
            .map(|n| n.to_string()))
    }

    fn parent_cell(&self, instance: &CellInstId) -> PyResult<CellId> {
        let id = self.chip.parent_cell(instance);
        Ok(CellId { id })
    }

    fn template_cell(&self, instance: &CellInstId) -> PyResult<CellId> {
        let id = self.chip.template_cell(instance);
        Ok(CellId { id })
    }

    // fn each_cell(&self) -> MyIterator {
    //     let cells = self.chip.each_cell_vec();
    //     let it = cells.into_iter()
    //         .map(|id| CellId {id});
    //     MyIterator {
    //         iter: Box::new(it)
    //     }
    // }

    fn each_cell(&self) -> Vec<CellId> {
        self.chip.each_cell().map(|id| CellId { id }).collect()
    }

    fn each_cell_instance(&self, parent: &CellId) -> Vec<CellInstId> {
        self.chip
            .each_cell_instance(parent)
            .map(|id| CellInstId { id })
            .collect()
    }

    fn each_cell_reference(&self, template: &CellId) -> Vec<CellInstId> {
        self.chip
            .each_cell_reference(template)
            .map(|id| CellInstId { id })
            .collect()
    }

    fn each_dependent_cell(&self, template: &CellId) -> Vec<CellId> {
        self.chip
            .each_dependent_cell(template)
            .map(|id| CellId { id })
            .collect()
    }

    fn each_cell_dependency(&self, template: &CellId) -> Vec<CellId> {
        self.chip
            .each_cell_dependency(template)
            .map(|id| CellId { id })
            .collect()
    }

    fn num_dependent_cells(&self, cell: &CellId) -> PyResult<usize> {
        Ok(self.chip.num_dependent_cells(cell))
    }

    fn num_cell_dependencies(&self, cell: &CellId) -> PyResult<usize> {
        Ok(self.chip.num_cell_dependencies(cell))
    }

    fn num_cell_references(&self, cell: &CellId) -> PyResult<usize> {
        Ok(self.chip.num_cell_references(cell))
    }

    // == NetlistBase & NetlistEdit ==

    fn create_pin(
        &mut self,
        parent: &CellId,
        pin_name: String,
        direction: Direction,
    ) -> PyResult<PinId> {
        let id = self
            .chip
            .create_pin(parent, pin_name.into(), direction.into());
        Ok(PinId { id })
    }

    fn create_net(&mut self, parent: &CellId, net_name: Option<String>) -> PyResult<NetId> {
        let id = self.chip.create_net(parent, net_name.map(|n| n.into()));
        Ok(NetId { id })
    }

    fn connect_pin(&mut self, pin: &PinId, net: Option<NetId>) -> PyResult<()> {
        self.chip.connect_pin(pin, net.map(|n| n.id));
        Ok(())
    }

    fn disconnect_pin(&mut self, pin: &PinId) -> PyResult<()> {
        self.chip.disconnect_pin(pin);
        Ok(())
    }

    fn connect_pin_instance(&mut self, pin_inst: &PinInstId, net: Option<NetId>) -> PyResult<()> {
        self.chip
            .connect_pin_instance(&pin_inst.id, net.map(|n| n.id));
        Ok(())
    }

    fn disconnect_pin_instance(&mut self, pin_inst: &PinInstId) -> PyResult<()> {
        self.chip.disconnect_pin_instance(&pin_inst.id);
        Ok(())
    }

    /// Get the ID of the template pin of this pin instance.
    fn template_pin(&self, pin_instance: &PinInstId) -> PyResult<PinId> {
        let id = self.chip.template_pin(&pin_instance.id);
        Ok(PinId { id })
    }

    /// Get the signal direction of the pin.
    fn pin_direction(&self, pin: &PinId) -> PyResult<Direction> {
        let d = self.chip.pin_direction(pin);
        Ok(d.into())
    }

    /// Get the name of the pin.
    fn pin_name(&self, pin: &PinId) -> PyResult<String> {
        Ok(self.chip.pin_name(pin))
    }

    /// Find a pin by its name.
    /// Returns `None` if no such pin can be found.
    fn pin_by_name(&self, parent_circuit: &CellId, name: &str) -> PyResult<Option<PinId>> {
        let id = self.chip.pin_by_name(&parent_circuit.id, name);
        Ok(id.map(|id| PinId { id }))
    }

    /// Get the ID of the parent circuit of this pin.
    fn parent_cell_of_pin(&self, pin: &PinId) -> PyResult<CellId> {
        let id = self.chip.parent_cell_of_pin(pin);
        Ok(CellId { id })
    }

    /// Get the ID of the circuit instance that holds this pin instance.
    fn parent_of_pin_instance(&self, pin_inst: &PinInstId) -> PyResult<CellInstId> {
        let id = self.chip.parent_of_pin_instance(&pin_inst.id);
        Ok(CellInstId { id })
    }

    /// Get the ID of a pin instance given the cell instance and the pin ID.
    fn pin_instance(&self, cell_inst: &CellInstId, pin: &PinId) -> PinInstId {
        // Inefficient default implementation.
        let id = self.chip.pin_instance(cell_inst, pin);
        PinInstId { id }
    }

    /// Get the ID of the parent circuit of this net.
    fn parent_cell_of_net(&self, net: &NetId) -> CellId {
        let id = self.chip.parent_cell_of_net(net);
        CellId { id }
    }

    /// Get the internal net attached to this pin.
    fn net_of_pin(&self, pin: &PinId) -> Option<NetId> {
        let id = self.chip.net_of_pin(pin);
        id.map(|id| NetId { id })
    }

    /// Get the external net attached to this pin instance.
    fn net_of_pin_instance(&self, pin_instance: &PinInstId) -> Option<NetId> {
        let id = self.chip.net_of_pin_instance(pin_instance);
        id.map(|id| NetId { id })
    }

    /// Get the net that is attached to this terminal.
    fn net_of_terminal(&self, terminal: &TerminalId) -> Option<NetId> {
        let id = self.chip.net_of_terminal(terminal);
        id.map(|id| NetId { id })
    }

    /// Get the net of the logical constant zero.
    fn net_zero(&self, parent_circuit: &CellId) -> NetId {
        let id = self.chip.net_zero(parent_circuit);
        NetId { id }
    }

    /// Get the net of the logical constant one.
    fn net_one(&self, parent_circuit: &CellId) -> NetId {
        let id = self.chip.net_one(parent_circuit);
        NetId { id }
    }

    /// Find a net by its name inside the parent circuit.
    /// Returns `None` if no such net can be found.
    fn net_by_name(&self, parent_circuit: &CellId, name: &str) -> Option<NetId> {
        let id = self.chip.net_by_name(parent_circuit, name);
        id.map(|id| NetId { id })
    }

    /// Get the name of the net.
    fn net_name(&self, net: &NetId) -> Option<String> {
        self.chip.net_name(net)
    }

    // /// Call a function for each pin of the circuit.
    // fn for_each_pin<F>(&self, circuit: &Self::CellId, f: F) where F: FnMut(Self::PinId) -> ();
    //
    // /// Get a `Vec` with the IDs of all pins of this circuit.
    // fn each_pin_vec(&self, circuit: &Self::CellId) -> Vec<Self::PinId> {
    //     let mut v = Vec::new();
    //     self.for_each_pin(circuit, |c| v.push(c.clone()));
    //     v
    // }
    //

    /// Iterate over all pins of a circuit.
    fn each_pin(&self, cell: &CellId) -> Vec<PinId> {
        self.chip.each_pin(cell).map(|id| PinId { id }).collect()
    }

    /// Iterate over all pins of a circuit.
    fn each_pin_instance(&self, cell_inst: &CellInstId) -> Vec<PinInstId> {
        self.chip
            .each_pin_instance(cell_inst)
            .map(|id| PinInstId { id })
            .collect()
    }

    /// Get a vector of all external nets connected to the circuit instance.
    /// A net might appear more than once.
    fn each_external_net(&self, cell_inst: &CellInstId) -> Vec<NetId> {
        self.chip
            .each_external_net(cell_inst)
            .map(|id| NetId { id })
            .collect()
    }

    /// Iterate over all defined nets inside a circuit.
    fn each_internal_net(&self, cell: &CellId) -> Vec<NetId> {
        self.chip
            .each_internal_net(cell)
            .map(|id| NetId { id })
            .collect()
    }

    /// Return the number of nets defined inside a cell.
    fn num_internal_nets(&self, cell: &CellId) -> usize {
        self.chip.num_internal_nets(cell)
    }

    /// Get the number of pins that are connected to this net.
    fn num_net_pins(&self, net: &NetId) -> usize {
        self.chip.num_net_pins(net)
    }

    /// Get the number of pin instances that are connected to this net.
    fn num_net_pin_instances(&self, net: &NetId) -> usize {
        self.chip.num_net_pin_instances(net)
    }

    /// Get the number of terminals that are connected to this net.
    fn num_net_terminals(&self, net: &NetId) -> usize {
        self.chip.num_net_terminals(net)
    }

    /// Iterate over all terminals of a net.
    fn each_terminal_of_net(&self, net: &NetId) -> Vec<TerminalId> {
        self.chip
            .each_terminal_of_net(net)
            .map(|id| TerminalId { id })
            .collect()
    }

    /// Iterate over all pins of a net.
    fn each_pin_of_net(&self, net: &NetId) -> Vec<PinId> {
        self.chip
            .each_pin_of_net(net)
            .map(|id| PinId { id })
            .collect()
    }

    /// Iterate over all pins of a net.
    fn each_pin_instance_of_net(&self, net: &NetId) -> Vec<PinInstId> {
        self.chip
            .each_pin_instance_of_net(net)
            .map(|id| PinInstId { id })
            .collect()
    }

    // == LayoutBase & LayoutEdit ==
    //
    // /// Set the distance unit used in this layout in 'pixels per micron'.
    // fn set_dbu(&mut self, dbu: Self::Coord) {} // TODO: Remove default implementation.
    //

    /// Create a layer or return an existing one.
    fn find_or_create_layer(&mut self, index: u32, datatype: u32) -> LayerId {
        let id = self.chip.find_or_create_layer(index, datatype);
        LayerId { id }
    }

    /// Create a new layer.
    /// Use `set_layer_name()` to define a name.
    fn create_layer(&mut self, index: u32, datatype: u32) -> LayerId {
        let id = self.chip.create_layer(index, datatype);
        LayerId { id }
    }

    /// Set the name of a layer or clear the layer name when passing `None`.
    /// This method should not change the ID of the layer.
    /// Returns the previous name of the layer.
    fn set_layer_name(&mut self, layer: &LayerId, name: Option<String>) -> Option<String> {
        self.chip.set_layer_name(layer, name.map(|n| n.into()))
    }

    // /// Insert a geometric shape into the parent cell.
    fn insert_shape(
        &mut self,
        parent_cell: &CellId,
        layer: &LayerId,
        geometry: Geometry,
    ) -> ShapeId {
        let id = self.chip.insert_shape(parent_cell, layer, geometry.into());
        ShapeId { id }
    }

    /// Remove shape from the parent cell.
    fn remove_shape(&mut self, shape_id: &ShapeId) -> Option<Geometry> {
        self.chip.remove_shape(shape_id).map(|g| (&g).into())
    }

    /// Replace the geometry of a shape.
    fn replace_shape(&mut self, shape_id: &ShapeId, geometry: Geometry) -> Geometry {
        (&self.chip.replace_shape(shape_id, geometry.into())).into()
    }

    /// Get the actual geometry data.
    fn get_shape(&mut self, shape_id: &ShapeId) -> Geometry {
        self.chip.with_shape(shape_id, |_, g| g.into())
    }

    /// Get the actual geometry data.
    fn get_layer_of_shape(&mut self, shape_id: &ShapeId) -> LayerId {
        self.chip.with_shape(shape_id, |&id, _| LayerId { id })
    }

    /// Set the geometric transform that describes the location of a cell instance relative to its parent.
    fn set_transform(&mut self, cell_inst: &CellInstId, tf: SimpleTransform) {
        self.chip.set_transform(cell_inst, tf.into())
    }

    /// Get the distance unit used in this layout in 'pixels per micron'.
    fn dbu(&self) -> i32 {
        self.chip.dbu()
    }

    /// Iterate over all defined layers.
    fn each_layer(&self) -> Vec<LayerId> {
        self.chip.each_layer().map(|id| LayerId { id }).collect()
    }

    /// Get the `LayerInfo` data structure for this layer.
    fn layer_info(&self, layer: &LayerId) -> LayerInfo {
        let info = self.chip.layer_info(layer);

        LayerInfo {
            index: info.index,
            data_type: info.datatype,
            name: info.name.as_ref().map(|n| n.to_string()),
        }
    }

    /// Find layer index by the (index, data type) tuple.
    fn find_layer(&self, index: u32, datatype: u32) -> Option<LayerId> {
        self.chip
            .find_layer(index, datatype)
            .map(|id| LayerId { id })
    }

    /// Find layer index by the name.
    fn layer_by_name(&self, name: &str) -> Option<LayerId> {
        self.chip.layer_by_name(name).map(|id| LayerId { id })
    }

    /// Compute the bounding box of the shapes on one layer.
    /// The bounding box also includes all child cell instances.
    fn bounding_box_per_layer(&self, cell: &CellId, layer: &LayerId) -> Option<Rect> {
        self.chip
            .bounding_box_per_layer(cell, layer)
            .map(|bbox| bbox.into())
    }

    /// Compute the bounding box of the cell over all layers.
    /// The bounding box is not defined if the cell is empty. In this
    /// case return `None`.
    fn bounding_box(&self, cell: &CellId) -> Option<Rect> {
        self.chip.bounding_box(cell).map(|bbox| bbox.into())
    }

    /// Iterate over the IDs of all shapes in the cell on a specific layer.
    fn each_shape_id(&self, cell: &CellId, layer: &LayerId) -> Vec<ShapeId> {
        self.chip
            .each_shape_id(cell, layer)
            .map(|id| ShapeId { id })
            .collect()
    }

    // /// Call a function for each shape on this layer.
    // fn for_each_shape<F>(&self, cell: &Self::CellId, layer: &Self::LayerId, f: F)
    //     where F: FnMut(&Self::ShapeId, &Geometry<Self::Coord>) -> ();
    //
    // /// Access a shape by its ID.
    // fn with_shape<F, R>(&self, shape_id: &Self::ShapeId, f: F) -> R
    //     where F: FnMut(&Self::LayerId, &Geometry<Self::Coord>) -> R;
    //

    /// Get the parent cell and the layer of a shape as a (cell, layer) tuple.
    fn parent_of_shape(&self, shape_id: &ShapeId) -> (CellId, LayerId) {
        let (cell, layer) = self.chip.parent_of_shape(shape_id);
        (CellId { id: cell }, LayerId { id: layer })
    }

    // /// Call a function `f` for each shape of this cell and its sub cells.
    // /// Along to the geometric shape `f` also gets a transformation as argument.
    // /// The transformation describes the actual position of the geometric shape relative to the `cell`.
    // fn for_each_shape_recursive<F>(&self, cell: &Self::CellId, layer: &Self::LayerId, mut f: F)
    //     where F: FnMut(SimpleTransform<Self::Coord>, &Self::ShapeId, &Geometry<Self::Coord>) -> () {
    //
    //     // This recursive iteration through the cells is implemented iteratively.
    //     // A plain recursive implementation is more difficult to handle due to the type system.
    //
    //     // Stack for resolved recursion.
    //     let mut stack = Vec::new();
    //     stack.push((cell.clone(), SimpleTransform::identity()));
    //
    //     while let Some((cell, tf)) = stack.pop() {
    //
    //         // Push child instances.
    //         self.for_each_cell_instance(&cell, |inst| {
    //             let template = self.template_cell(&inst);
    //             let transform = self.get_transform(&inst);
    //             let tf2 = transform.then(&tf);
    //             stack.push((template, tf2));
    //         });
    //
    //         // Process shapes of this cell.
    //         self.for_each_shape(&cell, layer,|id, g| f(tf.clone(), id, g));
    //     }
    // }
    //
    // /// Get the geometric transform that describes the location of a cell instance relative to its parent.
    // fn get_transform(&self, cell_inst: &Self::CellInstId) -> SimpleTransform<Self::Coord>;
    //
    // /// Get a property of a shape.
    // fn get_shape_property(&mut self, shape: &Self::ShapeId, key: &Self::NameType) -> Option<PropertyValue> {
    //     None
    // }

    // == L2NEdit & L2NBase ==

    /// Create the link between a circuit pin and its shapes in the layout.
    /// Return the current pin.
    fn set_pin_of_shape(&mut self, shape_id: &ShapeId, pin: Option<PinId>) -> Option<PinId> {
        self.chip
            .set_pin_of_shape(shape_id, pin.map(|id| id.id))
            .map(|id| PinId { id })
    }

    /// Set the net of a shape.
    /// Return the current net.
    fn set_net_of_shape(&mut self, shape_id: &ShapeId, net: Option<NetId>) -> Option<NetId> {
        self.chip
            .set_net_of_shape(shape_id, net.map(|id| id.id))
            .map(|id| NetId { id })
    }

    /// Iterate over all shapes that are marked to belong to the specified net.
    fn shapes_of_net(&self, net_id: &NetId) -> Vec<ShapeId> {
        self.chip
            .shapes_of_net(net_id)
            .map(|id| ShapeId { id })
            .collect()
    }

    /// Iterate over all shapes that are part of the pin.
    fn shapes_of_pin(&self, pin_id: &PinId) -> Vec<ShapeId> {
        self.chip
            .shapes_of_pin(pin_id)
            .map(|id| ShapeId { id })
            .collect()
    }

    /// Get the net of a shape.
    fn get_net_of_shape(&self, shape_id: &ShapeId) -> Option<NetId> {
        self.chip.get_net_of_shape(shape_id).map(|id| NetId { id })
    }

    /// Get the pin that belongs to the shape (if any).
    fn get_pin_of_shape(&self, shape_id: &ShapeId) -> Option<PinId> {
        self.chip.get_pin_of_shape(shape_id).map(|id| PinId { id })
    }
}
